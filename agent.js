require('dotenv').config();
const {exec} = require('child_process');
const http = require('http');
const hostname = process.env.server_host;
const serverPort = process.env.server_port;
const agentId = process.env.agent_string;
const token = process.env.agent_token;
const request_interval = (1000 * 60) * process.env.request_interval // min

function main() {
  try {
    iAmAlive();
  } catch (e) {
    console.error(e);
  }
  setInterval(function () {
    try {
      iAmAlive();
    } catch (e) {
      console.error(e);
    }
  }, request_interval)
}

function iAmAlive() {
  exec('curl icanhazip.com', (err, stdout, stderr) => {
    if (err) {
      console.error(err)
    } else {
      const data = JSON.stringify(
        {
          id: agentId,
          ipaddress: stdout.slice(0, -1),
          timestamp: new Date().toISOString(),
          interval: process.env.request_interval
        }
      );
      const options = {
        hostname: hostname,
        port: serverPort,
        path: '/',
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Content-Length': data.length,
          'Authorization': token
        }
      }
      const req = http.request(options, res => {
        res.on('data', d => {
        })
      })

      req.on('error', error => {
        console.error(error)
      })
      req.write(data)
      req.end()
    }
  });
}

// start loop
main();
