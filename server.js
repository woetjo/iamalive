require('dotenv').config();
const http = require('http');
const winston = require('winston');
const server_port = process.env.server_port;

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports: [
    new winston.transports.File({filename: 'iamalive.log'})
  ],
});

const server = http.createServer(function (request, response) {
  let data = [];
  let responseData;
  if (request.headers['authorization'] === process.env.agent_token) {
    if (request.method === "POST") {
      request.on('data', function (chunk) {
        data.push(chunk);
      }).on('end', function () {
        responseData = JSON.parse(Buffer.concat(data).toString('utf-8'))
        logger.log({
          level: 'info',
          id: responseData['id'] || 'no id',
          ipaddress: responseData['ipaddress'] || 'no ip',
          timestamp: responseData['timestamp'] || 'no timestamp'
        })
      })
      response.writeHead(200);
      response.end();
    } else {
      logger.log({
        level: 'warning',
        message: 'Unsupported http-request',
        timestamp: new Date().toISOString()
      })
      response.writeHead(500);
      response.end();
    }
  } else {
    logger.log({
      level: 'warning',
      message: 'Unauthorized request',
      timestamp: new Date().toISOString()
    })
    response.writeHead(401);
    response.end();
  }
});

server.listen(server_port);
